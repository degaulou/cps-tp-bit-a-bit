#ifndef __BIT_OPERATIONS_H__
#define __BIT_OPERATIONS_H__

#include <stdint.h>

int get_bit(int32_t number, int pos);

int32_t set_bit(int32_t number, int pos);

int32_t clr_bit(int32_t number, int pos);

#endif
