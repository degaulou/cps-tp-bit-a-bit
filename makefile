all: bitabit test_read test_write generate_sequence

CC=gcc
CFLAGS=-Wall -Werror -g

bitabit: bitabit.o bit_operations.o
	$(CC) -o $@ $^

test_read: test_read.o bfile.o
	$(CC) -o $@ $^

test_write: test_write.o bfile.o
	$(CC) -o $@ $^

generate_sequence: generate_sequence.o
	$(CC) -o $@ $^

clean:
	rm -rf generate_sequence bitabit test_read test_write *.o *~
