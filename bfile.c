#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "bfile.h"

/*
  file_ended
  variable globale qui indique si on a rencontré la fin du fichier
*/

int file_ended;

BFILE *bstart(FILE *fichier, const char *mode) {
  BFILE* bf = NULL;
  // on vérifie qu'un fichier a été ouvert
  if(fichier) {
    bf = malloc(sizeof(BFILE));
    // si l'allocation a réussi
    if(bf) {
      bf->file = fichier;
      bf->mode = mode;
      bf->r_buffer_left = 0;
      bf->w_buffer_length = 0;
      file_ended = 0;
    }
  }
  return bf;
}

int bstop(BFILE *fichier) {
  // on vérifie que le fichier n'est pas NULL
  if(fichier) {
    // si le tampon d'écriture n'est pas vide
    if(fichier->w_buffer_length > 0) {
      // on écrit dans le fichier 0xff suivi du nombre de bits restants à écrire
      fprintf(fichier->file, "%c%c", 0xff, fichier->w_buffer_length);
      // on remplit de 0 et cela entraînera une sortie vers le fichier
      for(int i = fichier->w_buffer_length; i < 8; i++) {
        bitwrite(fichier, 0);
      }
    }
    // s'il est vide, on termine par 0xff 0x00
    else {
      fprintf(fichier->file, "%c%c", 0xff, 0);
    }
    free(fichier);
  }
  return 0;
}

char bitread(BFILE *fichier) {
  // si le tampon de lecture est vide et que le fichier n'est pas fini
  if(fichier->r_buffer_left == 0 && !beof(fichier)) {
    // on remplit le tampon
    char cread;
    fscanf(fichier->file, "%c", &cread);
    for(int i = 0; i < 8; i++) {
      fichier->r_buffer[i] = (cread >> (7 - i)) & 1;
    }
    fichier->r_buffer_left = 8;

    // si on trouve notre début de directive 0xff
    if(cread == ~0 && !beof(fichier)) {
      // on reremplit le tampon
      fscanf(fichier->file, "%c", &cread);
      for(int i = 0; i < 8; i++) {
        fichier->r_buffer[i] = (cread >> (7 - i)) & 1;
      }
      fichier->r_buffer_left = 8;

      // si on trouve la suite de notre directive
      if(cread == 0) {
        // on arrête si c'est 0x00
        file_ended = 1;
        return -1;
      }
      if(0x00 < cread && cread < 0x08 && !beof(fichier)) {
        // il nous reste donc cread caractères à lire
        fichier->r_buffer_left = cread;

        // on lit le dernier octet du fichier
        fscanf(fichier->file, "%c", &cread);

        // on décale les bits qui nous intéressent tout à droite
        cread >>= (8 - fichier->r_buffer_left);

        // on copie les bits dans le tableau
        for(int i = 0; i < 8; i++) {
          fichier->r_buffer[i] = (cread >> (7 - i)) & 1;
        }
      }
    }
  }
  // on est arrivé à la fin du fichier, erreur de lecture
  else if(beof(fichier)) {
    return -1;
  }
  // on récupère le bit dans le tampon
  char bit = fichier->r_buffer[8 - fichier->r_buffer_left];
  fichier->r_buffer_left--;
  return bit;
}

int bitwrite(BFILE *fichier, char bit) {
  // on écrit le bit dans le tampon
  fichier->w_buffer[fichier->w_buffer_length] = bit & 1;
  fichier->w_buffer_length++;
  // si le tampon d'écriture est rempli
  if(fichier->w_buffer_length == 8) {
    // on sort le caractère et on vide le tampon
    char cwritten = 0;
    for(int i = 0; i < 8; i++) {
      cwritten |= fichier->w_buffer[i] << (7 - i);
    }
    fprintf(fichier->file, "%c", cwritten);
    // si le caractère écrit est 0xff
    if(cwritten == ~0) {
      // on le double
      fprintf(fichier->file, "%c", cwritten);
    }
    fichier->w_buffer_length = 0;
  }
  return 0;
}

int beof(BFILE *fichier) {
  return file_ended || feof(fichier->file);
}
