#include <stdint.h>
#include "bit_operations.h"

int get_bit(int32_t number, int pos) {
  return (number >> pos) & 1;
}

int32_t set_bit(int32_t number, int pos) {
  return number | (1 << pos);
}

int32_t clr_bit(int32_t number, int pos) {
  return number & ~(1 << pos);
}
